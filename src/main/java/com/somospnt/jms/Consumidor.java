package com.somospnt.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumidor {

    @JmsListener(destination = "demo.topic", selector = "nombre='poncio'")
    public void receiveQueue(String text) {
        System.out.println("Mensaje recibido: " + text);
    }

}
