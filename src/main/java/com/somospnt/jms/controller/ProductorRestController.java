package com.somospnt.jms.controller;

import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Topic;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductorRestController {

    private final Topic topic;
    private final JmsMessagingTemplate jmsMessagingTemplate;

    public ProductorRestController(Topic topic, JmsMessagingTemplate jmsMessagingTemplate) {
        this.topic = topic;
        this.jmsMessagingTemplate = jmsMessagingTemplate;
    }

    @RequestMapping("/api/enviar/{mensaje}")
    public void enviar(@PathVariable("mensaje") String mensaje) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("nombre", "poncio");
        jmsMessagingTemplate.convertAndSend(topic, mensaje, headers);
        System.out.println("Enviando mensaje: " + mensaje);
    }

}
